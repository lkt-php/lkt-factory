<?php


namespace Lkt\Factory\StoreData;


use Carbon\Carbon;
use chillerlan\Filereader\File;
use Lkt\Factory\FactorySettings;
use function Lkt\Factory\escapeDatabaseCharacters;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_EMAIL;
use const Lkt\Factory\COLUMN_FILE;
use const Lkt\Factory\COLUMN_FLOAT;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;
use const Lkt\Factory\COLUMN_FOREIGN_KEYS;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_IMAGE;
use const Lkt\Factory\COLUMN_INTEGER;
use const Lkt\Factory\COLUMN_JSON;
use const Lkt\Factory\COLUMN_RELATED_KEYS;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;

/**
 * Class StoreParser
 * @package Lkt\Factory\StoreData
 */
class StoreParser
{
    /**
     * @param $value
     * @param $column
     * @param $type
     * @return false|float|int|string|string[]|null
     */
    public static function parser($value, $column, $type)
    {
        $fields = FactorySettings::getComponentFields($type);
        $field = $fields[$column];
        $fieldType = $field['type'];
        $compress = $field['compress'] === true;

        switch ($fieldType) {
            case COLUMN_STRING:
            case COLUMN_EMAIL:
            case COLUMN_RELATED_KEYS:
            case COLUMN_FOREIGN_KEYS:
                $r = trim($value);
                if ($compress) {
                    return "COMPRESS('{$r}')";
                }
                return $r;

            case COLUMN_HTML:
                $r = escapeDatabaseCharacters($value);
                if ($compress) {
                    return "COMPRESS('{$r}')";
                }
                return $r;

            case COLUMN_BOOLEAN:
                return $value === true ? 1 : 0;

            case COLUMN_INTEGER:
            case COLUMN_FOREIGN_KEY:
                return (int)$value;

            case COLUMN_FLOAT:
                return (float)$value;

            case COLUMN_UNIX_TIMESTAMP:
                if ($value instanceof Carbon) {
                    return strtotime($value->format('Y-m-d H:i:s'));
                }
                return 0;

            case COLUMN_DATETIME:
                if ($value instanceof Carbon) {
                    return $value->format('Y-m-d H:i:s');
                }
                return '0000-00-00 00:00:00';

            case COLUMN_FILE:
            case COLUMN_IMAGE:
                if ($value instanceof File){
                    return $value->name;
                }
                return '';
            case COLUMN_JSON;
                if (is_array($value)){
                    $v = htmlspecialchars(json_encode($value), JSON_UNESCAPED_UNICODE|ENT_QUOTES, 'UTF-8');
                    $v = escapeDatabaseCharacters($v);

                    if ($compress) {
                        return "COMPRESS('{$v}')";
                    }
                    return $v;
                }
                return null;
            default:
                return null;
        }
    }
}