<?php

namespace Lkt\Factory\StoreData;

use Lkt\Drivers\MySql;
use Lkt\Factory\Builders\QueryBuilder;
use Lkt\Factory\Builders\WhereBuilder;
use Lkt\Factory\Cache\FactoryCache;
use Lkt\Factory\Factory\InstanceFactory;
use Lkt\Factory\LoadData\InstanceGenerator;
use Lkt\Factory\Settings\FactorySettings;
use function Lkt\Tools\Arrays\compareArrays;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;

/**
 * Trait StoreDataTrait
 * @package Lkt\Factory\StoreData
 */
trait StoreDataTrait
{
    public function save()
    {
        $isValid = true;
        $isUpdate = true;
        if($this->isAnonymous()){
            $validationClassName = FactorySettings::getComponentCreateValidationClassName(static::GENERATED_TYPE);
            if ($validationClassName) {
                $isValid = $validationClassName::getInstance($this);
            }
            $isUpdate = false;
        } else {
            $validationClassName = FactorySettings::getComponentUpdateValidationClassName(static::GENERATED_TYPE);
            if ($validationClassName) {
                $isValid = $validationClassName::getInstance($this);
            }
        }

        if (!$isValid) {
            return -1;
        }


        $fields = FactorySettings::getComponentFields(static::GENERATED_TYPE);
        $parsed = [];

        $schema = FactorySettings::getSchema(static::GENERATED_TYPE);

        $queryBuilder = QueryBuilder::table($schema->getTable(), static::GENERATED_TYPE);

        foreach ($fields as $column => $field) {
            $columnKey = $column;
            if ($field['type'] === COLUMN_FOREIGN_KEY) {
                $columnKey .= 'Id';
            }
            if (array_key_exists($columnKey, $this->UPDATED)){
                $parsed[$field['column']] = StoreParser::parser($this->UPDATED[$columnKey], $columnKey, static::GENERATED_TYPE);
            }
        }

        $queryBuilder->updateData($parsed);

        $query = MySql::makeUpdateParams($parsed);
        $origIdColumn = FactorySettings::getComponentIdColumn(static::GENERATED_TYPE);

//        if ($this->isAnonymous()) {
//            $query = MySql::makeInsertStatement(FactorySettings::getComponentTable(static::GENERATED_TYPE), $query);
//        } else {
//            $idColumn = FactorySettings::getComponentField(static::GENERATED_TYPE, $origIdColumn);
//            $idColumn = $idColumn['column'];
//            $query = MySql::makeUpdateStatement(FactorySettings::getComponentTable(static::GENERATED_TYPE), $query, MySql::makeUpdateParams([$idColumn => $this->DATA[$origIdColumn]]));
//        }

        if ($isUpdate) {
            $idColumn = FactorySettings::getComponentField(static::GENERATED_TYPE, $origIdColumn);
            $idColumn = $idColumn['column'];
            $queryBuilder
                ->where(WhereBuilder::integerEqual($idColumn, $this->DATA[$origIdColumn]));
            $query = $queryBuilder->getUpdateQuery();
        } else {
            $query = $queryBuilder->getInsertQuery();
        }

        $connection = FactorySettings::getConnection();
        $queryResponse = $connection->query($query);

        $id = (int)$connection->getLastInsertedId();
        $reload = true;

        if ($id > 0 && !$this->DATA[$origIdColumn]) {
            $this->DATA[$origIdColumn] = $id;

        } elseif($this->DATA[$origIdColumn] > 0) {
            $id = $this->DATA[$origIdColumn];
        }

        if ($queryResponse !== false){
            foreach ($this->UPDATED as $k => $v){
                $this->DATA[$k] = $v;
                unset($this->UPDATED[$k]);
            }
        }

        if (count($this->PENDING_UPDATE_RELATED_DATA) > 0){
            foreach ($this->PENDING_UPDATE_RELATED_DATA as $column => $data){
                $field = FactorySettings::getComponentField(static::GENERATED_TYPE, $column);
                $relatedComponent = $field['component'];
                $relatedCrud = FactorySettings::getComponentCrudConfig($relatedComponent);
                $relatedIdColumn = FactorySettings::getComponentIdColumn($relatedComponent);
                $relatedIdColumnGetter = 'get'.ucfirst($relatedIdColumn);
                $relatedClass = FactorySettings::getComponentClassName($relatedComponent);

                $create = $relatedCrud['create'];
                $update = $relatedCrud['update'];
                $delete = $relatedCrud['delete'];

                // Check which items must be deleted
                $currentItems = $this->_getRelatedVal($relatedComponent, $column, true);
                $currentIds = [];
                foreach ($currentItems as $currentItem){
                    $idAux = (int)$currentItem->{$relatedIdColumnGetter}();
                    if ($idAux > 0 && !in_array($idAux, $currentIds, true)){
                        $currentIds[] = $idAux;
                    }
                }

                $updatedIds = [];
                foreach ($data as $datum){
                    if ($datum[$relatedIdColumn] > 0){
                        $updatedIds[] = (int)$datum[$relatedIdColumn];
                    }
                }

                $diff = compareArrays($currentIds, $updatedIds);

                // Delete
                foreach ($diff['deleted'] as $deletedId) {
                    $delete::getInstance($relatedClass::getInstance($deletedId));
                }


                // Update or create
                foreach ($data as $datum){
                    if ($datum[$relatedIdColumn] > 0){
                        $instance = $relatedClass::getInstance($datum[$relatedIdColumn]);
                        $update::getInstance($instance, $datum);

                    } else {
                        $create::getInstance($datum);
                    }
                }
            }
            $this->PENDING_UPDATE_RELATED_DATA = [];
        }

        if ($reload) {
            $cacheCode = "{$this->TYPE}_{$id}";
            InstanceFactory::getInstance($this->TYPE, $id)->query();


            $class = FactorySettings::getComponentClassName($this->TYPE);
            if($class === get_class($this)){
                InstanceGenerator::store($cacheCode, $this);
            }

            if (FactoryCache::inCache($cacheCode)) {
                $data = FactoryCache::load($cacheCode);
                foreach ($data as $key => $datum) {
                    $this->DATA[$key] = $datum;
                }
            }

            $this->RELATED_DATA = [];
        }

        return $id;
    }

    public function delete()
    {
        if ($this->isAnonymous()){
            return 1;
        }

        $origIdColumn = FactorySettings::getComponentIdColumn(static::GENERATED_TYPE);
        $idColumn = FactorySettings::getComponentField(static::GENERATED_TYPE, $origIdColumn);
        $idColumn = $idColumn['column'];
        $query = MySql::makeDeleteStatement(FactorySettings::getComponentTable(static::GENERATED_TYPE), MySql::makeUpdateParams([$idColumn => $this->DATA[$origIdColumn]]));
        $connection = FactorySettings::getConnection();
        $queryResponse = $connection->query($query);
        if ($queryResponse === true) {
            $code = static::GENERATED_TYPE . '_' . $this->DATA[$origIdColumn];
            InstanceGenerator::clearCode($code);
            return 1;
        }
        return -1;
    }
}