<?php


namespace Lkt\Factory\LoadData;


use Lkt\Factory\Cache\FactoryCache;
use Lkt\Factory\Settings\FactorySettings;
use Lkt\Factory\ValidateData\DataValidator;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class QueryBuilder
 * @package Lkt\Factory\LoadData
 */
class QueryBuilder
{
    use InstantiableTrait;

    public $moduleType = 0;
    public $itemId = 0;

    public $select = '';
    public $selectAppend = '';

    public $from = '';
    public $fromAppend = '';

    public $where = '1';
    public $whereAppend = '';

    public $orderBy = '';

    public $page = -1;
    public $limit = -1;

    public $isSingleMode = false;
    public $singleWithoutPrimaryKey = false;

    public $rawQuery = '';
    public $forceRefresh = false;

    /**
     * @return bool
     */
    protected function IsSingle()
    {
        return $this->isSingleMode
            || $this->limit === 1;
    }

    /**
     * @return array|bool
     */
    protected function getRawResults()
    {
        $connection = FactorySettings::getConnection();
        if (method_exists($connection, 'forceRefresh')) {
            $connection->forceRefresh($this->forceRefresh);
        }
        $r = $connection->query($this->getQuery());
        if (method_exists($connection, 'forceRefresh')) {
            $connection->forceRefresh(false);
        }
        return $r;
    }

    /**
     * @return bool
     */
    protected function execute()
    {
        $connection = FactorySettings::getConnection();
        return $connection->query($this->getQuery());
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        if ($this->rawQuery !== '') {
            return $this->rawQuery;
        }

        $from = $this->from;
        $where = $this->where;
        $select = $this->select;

        $isDelete = strpos(strtolower($select), 'delete') === 0;

        if ($select === ''){
            $select = QueryColumnController::getInstance($this->moduleType);
        }

        if ($this->fromAppend !== '') {
            $from .= $this->fromAppend;
        }

        if ($this->whereAppend !== '') {
            $where .= ' ' . $this->whereAppend;
        }

        if ($isDelete) {
            $query = "{$select} FROM {$from} WHERE {$where}";
        } else {
            $query = "SELECT DISTINCT {$select} FROM {$from} WHERE {$where}";
        }

        if (!$isDelete && $this->orderBy !== '') {
            $query .= "
            ORDER BY {$this->orderBy}";
        }

        if (!$isDelete && $this->page > -1 && $this->limit > -1) {
            $p = $this->page * $this->limit;
            $query .= "
            LIMIT {$p}, {$this->limit}";

        } elseif (!$isDelete && $this->limit > -1) {
            $query .= "
            LIMIT {$this->limit}";
        }

        return $query;
    }

    public function loadResults()
    {
        $results = $this->getRawResults();
        if (\count($results) === 0) {
            if ($this->IsSingle()) {
                return null;
            }
            return [];
        }

        $idColumn = FactorySettings::getComponentIdColumn($this->moduleType);
        foreach ($results as $iAux) {
            $id = (int)$iAux[$idColumn];
            DataValidator::getInstance($this->moduleType, $iAux);
            FactoryCache::store("{$this->moduleType}_{$id}", DataValidator::getResult());
        }
    }

    /**
     * @return array|mixed|null
     */
    public function getResults()
    {
        $results = $this->getRawResults();
        if (\count($results) === 0) {
            if ($this->IsSingle()) {
                return null;
            }
            return [];
        }

        $idColumn = FactorySettings::getComponentIdColumn($this->moduleType);
        foreach ($results as $iAux) {
            if (is_array($idColumn)) {
                $id = [];
                foreach ($idColumn as $t) {
                    $id[] = $iAux[$t];
                }

                $id = implode('-', $id);
            } else {
                $id = (int)$iAux[$idColumn];
            }
            DataValidator::getInstance($this->moduleType, $iAux);
            FactoryCache::store("{$this->moduleType}_{$id}", DataValidator::getResult());
        }

        if ($this->IsSingle()) {
            return [$results[0]];
        }
        return $results;
    }

    /**
     * @param string $countableField
     * @return int
     */
    public function getCount($countableField = '')
    {
        $this->select = "COUNT($countableField) as Count";
        $r = $this->getRawResults();
        return (int)$r[0]['Count'];
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $this->select = "DELETE";
        return $this->execute();
    }

    /**
     * @param string $countableField
     * @return string
     */
    public function getCountQuery($countableField = '')
    {
        $this->select = "COUNT($countableField) as Count";
        return $this->getQuery();
    }

    /**
     * @param int $moduleType
     * @return $this
     */
    public function setModule($moduleType = 0)
    {
        $this->moduleType = $moduleType;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setSelect($q = '')
    {
        $this->select = $q;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setFrom($q = '')
    {
        $this->from = $q;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setFromAppend($q = '')
    {
        $this->fromAppend = $q;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setWhere($q = '')
    {
        $this->where = $q;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setWhereAppend($q = '')
    {
        $this->whereAppend = $q;
        return $this;
    }

    /**
     * @param string $q
     * @return $this
     */
    public function setOrderBy($q = '')
    {
        $this->orderBy = $q;
        return $this;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage($page = 1)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit = 1)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setForceRefresh($status = true)
    {
        $this->forceRefresh = $status;
        return $this;
    }

    /**
     * @param bool $bool
     * @return $this
     */
    public function setSingleWithoutPrimaryKey($bool = true)
    {
        $this->singleWithoutPrimaryKey = $bool;
        return $this;
    }

    /**
     * @param string $sql
     * @return $this
     */
    public function setRaw($sql = '')
    {
        $this->rawQuery = $sql;
        return $this;
    }

}