<?php


namespace Lkt\Factory\LoadData;


use Lkt\Factory\FactorySettings;
use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;


class InstanceGenerator implements CacheControllerInterface
{
    use AutomaticInstanceTrait,
        InstantiableTrait,
        CacheControllerTrait;

    protected $type;
    protected $id;
    protected $forceRefresh;

    /**
     * InstanceGenerator constructor.
     * @param $type
     * @param $id
     * @param boolean $forceRefresh
     */
    public function __construct($type, $id, bool $forceRefresh = false)
    {
        $this->type = $type;
        $this->id = $id;
        $this->forceRefresh = $forceRefresh;
    }

    /**
     * @return mixed|null
     */
    public function handle()
    {
        $cacheCode = "{$this->type}_{$this->id}";
        if (!$this->forceRefresh && static::inCache($cacheCode)) {
            return static::load($cacheCode);
        }

        $class = FactorySettings::getComponentClassName($this->type);
        $r = $class::getInstance($this->id, $this->type);
        static::store($cacheCode, $r);
        return static::load($cacheCode);
    }
}