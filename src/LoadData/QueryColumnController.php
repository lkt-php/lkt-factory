<?php

namespace Lkt\Factory\LoadData;

use Lkt\Factory\Settings\FactorySettings;
use Lkt\InstancePatterns\AbstractInstances\AbstractAutomaticParserInstance;
use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;
use const Lkt\Factory\COLUMN_PIVOT;
use const Lkt\Factory\COLUMN_RELATED;
use const Lkt\Factory\COLUMN_RELATED_KEYS;


class QueryColumnController extends AbstractAutomaticParserInstance implements CacheControllerInterface
{
    use InstantiableTrait,
        AutomaticInstanceTrait,
        CacheControllerTrait;

    const SKIP_COLUMNS = [
        COLUMN_PIVOT,
        COLUMN_RELATED,
        COLUMN_RELATED_KEYS
    ];

    protected $code;
    protected $cacheCode;

    /**
     * QueryColumnController constructor.
     * @param $code
     */
    public function __construct($code)
    {
        $this->code = $code;
        $this->cacheCode = trim($code);
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        if (static::inCache($this->cacheCode)){
            return static::load($this->cacheCode);
        }

        $schema = FactorySettings::getSchema($this->code);
        if (!$schema) {
            return '';
        }
        
        $table = $schema->getTable();
        $fields = $schema->getAllFields();
        
        $r = [];
        
        foreach ($fields as $key => $field) {
            if (!in_array($field['type'], static::SKIP_COLUMNS, true)) {
                $column = trim($field['column']);
                if ($field['compress'] === true){
                    $r[] = "UNCOMPRESS({$table}.{$column}) as {$key}";
                } else {
                    $r[] = "{$table}.{$column} as {$key}";
                }
            }
        }

        $r = implode(',', $r);
        static::store($this->cacheCode, $r);
        return $r;
    }
}