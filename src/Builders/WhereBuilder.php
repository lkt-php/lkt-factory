<?php

namespace Lkt\Factory\Builders;

use Lkt\InstancePatterns\Traits\InstantiableTrait;
use function Lkt\Factory\buildColumnString;

class WhereBuilder
{
    use InstantiableTrait;

    protected $constraints = [];
    protected $or = [];
    protected $and = [];

    /**
     * @return string
     */
    public function __toString(): string
    {
        $r = [];
        foreach ($this->constraints as $constraint) {
            $r[] = $constraint;
        }

        foreach ($this->and as $constraint) {
            $r[] = (string)$constraint;
        }

        $or = [];
        foreach ($this->or as $constraint) {
            $or[] = (string)$constraint;
        }

        $data = [];

        if (isset($r[0])) {
            $data[] = implode(' AND ', $r);
        }

        if (isset($or[0])) {
            $data[] = implode(' OR ', $or);
        }

        return '(' . implode(' OR ', $data) . ')';
    }

    /**
     * @param string $column
     * @param string $value
     * @return $this
     */
    public function addStringEqual(string $column, string $value): self
    {
        $v = addslashes(stripslashes($value));
        if (strpos($value, 'COMPRESS(') === 0) {
            $this->constraints[] = "{$column}={$value}";
        } else {
            $this->constraints[] = "{$column}='{$v}'";
        }

        return $this;
    }

    public static function stringEqual(string $column, string $value): self
    {
        return (new WhereBuilder())->addStringEqual($column, $value);
    }


    /**
     * @param string $column
     * @param string $value
     * @return $this
     */
    public function addStringLike(string $column, string $value): self
    {
        $v = addslashes(stripslashes($value));
        $this->constraints[] = "{$column} LIKE '%{$v}%'";
        return $this;
    }

    public static function stringLike(string $column, string $value): self
    {
        return (new WhereBuilder())->addStringLike($column, $value);
    }

    /**
     * @param string $column
     * @param $value
     * @return $this
     */
    public function addIntegerEqual(string $column, $value): self
    {
        $v = addslashes(stripslashes((int)$value));
        $this->constraints[] = "{$column}={$v}";
        return $this;
    }

    /**
     * @param string $column
     * @param $value
     * @return $this
     */
    public function orIntegerEqual(string $column, $value): self
    {
        $v = addslashes(stripslashes((int)$value));
        $this->or[] = "{$column}={$v}";
        return $this;
    }

    /**
     * @param string $column
     * @param $value
     * @return static
     */
    public static function integerEqual(string $column, $value): self
    {
        return (new WhereBuilder())->addIntegerEqual($column, $value);
    }

    /**
     * @param string $column
     * @param $value
     * @return $this
     */
    public function addIntegerNot(string $column, $value): self
    {
        $v = addslashes(stripslashes((int)$value));
        $this->constraints[] = "{$column}!={$v}";
        return $this;
    }

    /**
     * @param string $column
     * @param $value
     * @return static
     */
    public static function integerNot(string $column, $value): self
    {
        return (new WhereBuilder())->addIntegerNot($column, $value);
    }

    /**
     * @param string $column
     * @param $value
     * @return $this
     */
    public function addDecimalEqual(string $column, $value): self
    {
        $v = addslashes(stripslashes((float)$value));
        $this->constraints[] = "{$column}={$v}";
        return $this;
    }

    /**
     * @param string $column
     * @param $value
     * @return static
     */
    public static function decimalEqual(string $column, $value): self
    {
        return (new WhereBuilder())->addDecimalEqual($column, $value);
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return $this
     */
    public function addSubQueryEqual(QueryBuilder $builder, $value): self
    {
        $column = '(' . $builder->getSelectQuery() . ')';
        $v = addslashes(stripslashes((float)$value));
        $this->constraints[] = "{$column}={$v}";
        return $this;
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return $this
     */
    public function addSubQueryIn(QueryBuilder $builder, $value): self
    {
        $column = '(' . $builder->getSelectQuery() . ')';
        $v = addslashes(stripslashes($value));
        $this->constraints[] = "{$column} IN ({$v})";
        return $this;
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return $this
     */
    public function addColumnInSubQuery(QueryBuilder $builder, $value): self
    {
        $column = '(' . $builder->getSelectQuery() . ')';
        $v = addslashes(stripslashes($value));
        $this->constraints[] = "{$v} IN ({$column})";
        return $this;
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return static
     */
    public static function subQueryEqual(QueryBuilder $builder, $value): self
    {
        return (new WhereBuilder())->addSubQueryEqual($builder, $value);
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return static
     */
    public static function subQueryIn(QueryBuilder $builder, $value): self
    {
        return (new WhereBuilder())->addSubQueryIn($builder, $value);
    }

    /**
     * @param QueryBuilder $builder
     * @param $value
     * @return static
     */
    public static function columnInSubQuery($value, QueryBuilder $builder): self
    {
        return (new WhereBuilder())->addColumnInSubQuery($builder, $value);
    }

    /**
     * @param WhereBuilder $builder
     * @return $this
     */
    public function orWhere(WhereBuilder $builder): self
    {
        $this->or[] = $builder;
        return $this;
    }

    /**
     * @param WhereBuilder $builder
     * @return $this
     */
    public function andWhere(WhereBuilder $builder): self
    {
        $this->and[] = $builder;
        return $this;
    }
}