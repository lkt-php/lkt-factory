<?php

namespace Lkt\Factory\Builders;

class JoinBuilder
{
    protected $direction = '';
    protected $table = '';
    protected $on = [];

    /**
     * @param string $table
     */
    public function __construct(string $table)
    {
        $this->table = $table;
    }

    /**
     * @param string $direction
     * @return $this
     */
    protected function setDirection(string $direction = 'left join'): JoinBuilder
    {
        $this->direction = $direction;
        return $this;
    }


    /**
     * @param string $table
     * @return JoinBuilder
     */
    public static function leftJoin(string $table): JoinBuilder
    {
        return (new JoinBuilder($table))->setDirection('left join');
    }


    /**
     * @param string $table
     * @return JoinBuilder
     */
    public static function rightJoin(string $table): JoinBuilder
    {
        return (new JoinBuilder($table))->setDirection('right join');
    }

    /**
     * @param string $originTableColum
     * @param string $joinedTableColumn
     * @return $this
     */
    public function on(string $originTableColum, string $joinedTableColumn): self
    {
        $this->on[] = [$originTableColum, $joinedTableColumn];
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $direction = strtoupper($this->direction);

        $on = [];
        foreach ($this->on as $value) {
            $on[] = "{{---LKT_PARENT_TABLE---}}.{$value[0]} = {$this->table}.{$value[1]}";
        }
        $strOn = implode(' AND ', $on);
        if ($strOn !== '') {
            $strOn = "ON ({$strOn})";
        }
        return "{$direction} {$this->table} {$strOn}";
    }
}