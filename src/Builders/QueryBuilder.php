<?php

namespace Lkt\Factory\Builders;

use Lkt\Drivers\MySql;
use Lkt\Factory\Settings\FactorySettings;
use function Lkt\Factory\buildColumnString;
use function Lkt\Tools\Pagination\getTotalPages;

class QueryBuilder
{
    protected $component = '';

    protected $columns = [];
    protected $table = '';
    protected $where = [];
    protected $data = [];

    protected $constraints = [];
    protected $orderBy = '';

    protected $page = -1;
    protected $limit = -1;

    protected $joins = [];
    protected $unions = [];

    /**
     * @param QueryBuilder $builder
     * @param string $alias
     * @return $this
     */
    final public function union(QueryBuilder $builder, string $alias): self
    {
        $this->unions[$alias] = $builder;
        return $this;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return $this
     */
    final public function pagination(int $page = 0, int $limit = 0): self
    {
        if ($page < 1) {
            $page = 1;
        }
        --$page;
        $this->page = $page;
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param string $orderBy
     * @return $this
     */
    final public function orderBy(string $orderBy): self
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    final public function setColumns(array $columns): self
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * @param string $table
     * @return $this
     */
    final protected function setTable(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @param string $component
     * @return $this
     */
    final protected function setComponent(string $component): self
    {
        $this->component = $component;
        return $this;
    }

    public static function table(string $table, string $component): self
    {
        return (new QueryBuilder())->setTable($table)->setComponent($component);
    }

    /**
     * @param array $data
     * @return $this
     */
    final public function updateData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param WhereBuilder $where
     * @return $this
     */
    final public function where(WhereBuilder $where): self
    {
        $this->where[] = $where;
        return $this;
    }

    /**
     * @param string $where
     * @return $this
     */
    final public function constraint(string $where): self
    {
        $this->constraints[] = $where;
        return $this;
    }

    /**
     * @param JoinBuilder $join
     * @return $this
     */
    final public function join(JoinBuilder $join): self
    {
        $this->joins[] = $join;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueryWhere(): string
    {
        $where = [];
        foreach ($this->where as $value) {
            $where[] = (string)$value;
        }
        foreach ($this->constraints as $value) {
            $where[] = $value;
        }

        $whereString = '';
        if (isset($where[0])) {
            $whereString = ' AND ' . implode(' AND ', $where);
        }

        return $whereString;
    }

    /**
     * @param string $type
     * @param string $countableField
     * @return string
     */
    private function getQuery(string $type, string $countableField = ''): string
    {
        $whereString = $this->getQueryWhere();

        switch ($type) {
            case 'select':
            case 'selectDistinct':
            case 'count':
                $from = [];
                foreach ($this->joins as $join) {
                    $from[] = (string)$join;
                }
                $fromString = implode(' ', $from);
                $fromString = str_replace('{{---LKT_PARENT_TABLE---}}', $this->table, $fromString);

                $distinct = '';

                if ($type === 'selectDistinct') {
                    $distinct = 'DISTINCT';
                    $type = 'select';
                }

                if ($type === 'select') {
                    $columns = $this->buildColumns();
                    $orderBy = '';
                    $pagination = '';

                    if ($this->orderBy !== '') {
                        $orderBy = " ORDER BY {$this->orderBy}";
                    }

                    if ($this->page > -1 && $this->limit > -1) {
                        $p = $this->page * $this->limit;
                        $pagination = " LIMIT {$p}, {$this->limit}";

                    } elseif ($this->limit > -1) {
                        $pagination = " LIMIT {$this->limit}";
                    }


                    return "SELECT {$distinct} {$columns} FROM {$this->table} {$fromString} WHERE 1 {$whereString} {$orderBy} {$pagination}";
                }

                if ($type === 'count') {
                    return "SELECT COUNT({$countableField}) AS Count FROM {$this->table} {$fromString} WHERE 1 {$whereString}";
                }
                return '';

            case 'update':
            case 'insert':
                $data = MySql::makeUpdateParams($this->data);

                if ($type === 'update') {
                    return "UPDATE {$this->table} SET {$data} WHERE 1 {$whereString}";
                }

                if ($type === 'insert') {
                    return "INSERT INTO {$this->table} SET {$data}";
                }
                return '';

            default:
                return '';
        }
    }

    /**
     * @return string
     */
    final public function getSelectQuery(): string
    {
        return $this->getQuery('select');
    }

    /**
     * @return string
     */
    final public function getSelectDistinctQuery(): string
    {
        return $this->getQuery('selectDistinct');
    }

    /**
     * @return string
     */
    final public function getCountQuery(): string
    {
        return $this->getQuery('count');
    }

    /**
     * @return string
     */
    final public function getInsertQuery(): string
    {
        return $this->getQuery('insert');
    }

    /**
     * @return string
     */
    final public function getUpdateQuery(): string
    {
        return $this->getQuery('update');
    }

    /**
     * @return array
     */
    final public function select(): array
    {
        $connection = FactorySettings::getConnection();
        $r = $connection->query($this->getQuery('select'));

        if (!is_array($r)) {
            $r = [];
        }
        return $r;
    }

    /**
     * @return array
     */
    final public function selectDistinct(): array
    {
        $connection = FactorySettings::getConnection();
        return $connection->query($this->getQuery('selectDistinct'));
    }

    /**
     * @param string $countableField
     * @return int
     */
    final public function count(string $countableField): int
    {
        $connection = FactorySettings::getConnection();
        $results = $connection->query($this->getQuery('count', $countableField));
        return (int)$results[0]['Count'];
    }

    /**
     * @param string $countableField
     * @return int
     */
    final public function pages(string $countableField): int
    {
        return getTotalPages($this->count($countableField), $this->limit);
    }

    /**
     * @return bool
     */
    final public function insert(): bool
    {
        $connection = FactorySettings::getConnection();
        return $connection->query($this->getQuery('insert'));
    }

    /**
     * @return bool
     */
    final public function update(): bool
    {
        $connection = FactorySettings::getConnection();
        return $connection->query($this->getQuery('update'));
    }

    /**
     * @return string
     */
    private function buildColumns(): string
    {
        $r = [];
        foreach ($this->columns as $column) {
            $r[] = buildColumnString($column, $this->table);
        }

        return implode(',', $r);
    }
}