<?php

namespace Lkt\Factory\ColumnTypeTraits;

use Lkt\Drivers\MySql;
use Lkt\Factory\Settings\FactorySettings;
use function Lkt\Factory\factory;
use function Lkt\Factory\schemaColumn;
use function Lkt\Factory\schemaIdColumn;
use function Lkt\Tools\Arrays\arrayPushUnique;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;


trait ColumnPivotTrait
{
    /**
     * @param string $column
     * @return void
     */
    private function _loadPivots(string $column)
    {
        $schema = FactorySettings::getSchema(static::GENERATED_TYPE);
        $field = $schema->getField($column);
        $idColumn = $schema->getIdColumn(true);

        $pivotedSchema = FactorySettings::getSchema($field['pivot']);
        $pivotedField = $pivotedSchema->getFieldsPointingToComponent(static::GENERATED_TYPE, true);

        $pivotedFieldColumn = trim($pivotedField['column']);

        $where = $field['where'];
        if (!is_array($where)){
            $where = [];
        }
        $where[] = MySql::makeUpdateParams([$pivotedFieldColumn => $this->DATA[$idColumn]]);

        $order = $field['order'];
        if (!is_array($order)){
            $order = [];
        }
        $pivots = factory($field['pivot'])
            ->where(implode(' AND ', $where))
            ->orderBy(implode(',', $order))
            ->query();

        $this->PIVOT[$column] = $pivots;
    }


    /**
     * @param string $column
     * @return array
     */
    protected function _getPivotVal(string $column) :array
    {
        if (!isset($this->PIVOT[$column])) {
            $this->_loadPivots($column);
        }

        if (isset($this->UPDATED_PIVOT_DATA[$column])) {
            return $this->UPDATED_PIVOT_DATA[$column];
        }

        if (isset($this->PIVOT_DATA[$column])) {
            return $this->PIVOT_DATA[$column];
        }

        $field = schemaColumn(static::GENERATED_TYPE, $column);
        $fieldPivotColumn = FactorySettings::getComponentFieldPointingToComponent($field['pivot'], $field['component']);
        $key = FactorySettings::getComponentFieldKey($field['pivot'], $fieldPivotColumn['column']);
        $getter = 'get'.ucfirst($key);

        if ($fieldPivotColumn['type'] === COLUMN_FOREIGN_KEY) {
            $getter .= 'Id';
        }

        $ids = array_map(function($item) use ($getter) {
            return $item->{$getter}();
        }, $this->PIVOT[$column]);

        if (count($ids) === 0) {
            $this->PIVOT_DATA[$column] = [];
            return [];
        }

        $idColumn = schemaIdColumn($field['component']);
        $idColumnConfig = schemaColumn($field['component'], $idColumn);
        $where = $idColumnConfig['column'] . ' IN (' . implode(',', $ids) . ')';
        $order = [];
        foreach ($ids as $id){
            arrayPushUnique($order, "{$idColumnConfig['column']} = '{$id}' DESC");
        }

        $order = trim(implode(', ', $order));

        $r = factory($field['component'])
            ->where($where)
            ->orderBy($order)
            ->query();

        if (!is_array($r)) {
            $r = [];
        }

        $this->PIVOT_DATA[$column] = $r;
        return $this->PIVOT_DATA[$column];
    }

    /**
     * @param string $type
     * @param string $column
     * @return bool
     */
    protected function _hasPivotVal($type = '', $column = '') :bool
    {
        return count($this->_getRelatedVal($type)) > 0;
    }

//    protected function _setPivotVal($type = '', $column = '', $items = [], $deleteUnlinked = false)
//    {
//        $this->UPDATED[$column] = $items;
//    }
}