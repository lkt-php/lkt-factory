<?php

namespace Lkt\Factory\ColumnTypeTraits;

use Lkt\Factory\ValidateData\DataValidator;

/**
 * Trait ColumnStringTrait
 * @package Lkt\Factory\ColumnTypeTraits
 */
trait ColumnBooleanTrait
{
    /**
     * @param string $field
     * @return bool
     */
    protected function _getBooleanVal(string $field) :bool
    {
        if (isset($this->UPDATED[$field])) {
            return $this->UPDATED[$field];
        }
        return $this->DATA[$field] === true;
    }

    /**
     * @param string $field
     * @param bool $value
     */
    protected function _setBooleanVal(string $field, bool $value = false)
    {
        $checkField = 'has'.ucfirst($field);
        DataValidator::getInstance($this->TYPE, [
            $field => $value,
        ]);
        $this->UPDATED = $this->UPDATED + DataValidator::getResult();
    }
}