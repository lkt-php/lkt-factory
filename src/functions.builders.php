<?php

namespace Lkt\Factory;

function buildColumnString(string $column, string $table): string
{
    $prependTable = "{$table}.";
    $tempColumn = str_replace([' as ', ' AS ', ' aS ', ' As '], '{{---LKT_SEPARATOR---}}', $column);
    $exploded = explode('{{---LKT_SEPARATOR---}}', $tempColumn);

    $key = trim($exploded[0]);
    $alias = trim($exploded[1]);

    if (strpos($column, 'UNCOMPRESS') === 0) {
        if ($alias !== '') {
            $r = "{$key} AS {$alias}";
        } else {
            $r = $key;
        }
    }

    elseif (strpos($key, $prependTable) === 0) {
        if ($alias !== '') {
            $r = "{$key} AS {$alias}";
        } else {
            $r = $key;
        }
    } else {
        if ($alias !== '') {
            $r = "{$prependTable}{$key} AS {$alias}";
        } else {
            $r = "{$prependTable}{$key}";
        }
    }

    return $r;
}