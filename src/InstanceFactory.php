<?php

namespace Lkt\Factory;

use Lkt\Factory\LoadData\InstanceGenerator;
use Lkt\Factory\LoadData\QueryBuilder;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class InstanceFactory
 * @package Lkt\Factory
 * @deprecated
 */
class InstanceFactory extends \Lkt\Factory\Factory\InstanceFactory
{
}