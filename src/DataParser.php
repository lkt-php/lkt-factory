<?php


namespace Lkt\Factory;

use Carbon\Carbon;
use chillerlan\Filereader\Directory;
use chillerlan\Filereader\Drivers\DiskDriver;
use chillerlan\Filereader\File;

/**
 * Class DataParser
 * @package Lkt\Factory
 * @deprecated
 */
class DataParser
{
    protected static $DISK_DRIVER;
    protected static $DECIMAL_NUMBER_FORMATTER;

    /**
     * @param $value
     * @return string
     */
    public static function prepareStringField($value)
    {
        return addslashes(stripslashes(trim($value)));
    }

    /**
     * @param string $str
     * @param array $opts
     * @param int $defaultIndex
     * @return string
     */
    public static function ensureStringInOptions(string $str, array $opts, int $defaultIndex = 0): string
    {
        if (in_array($str, $opts, true)){
            return $str;
        }
        return $opts[$defaultIndex];
    }

    /**
     * @param $number
     * @param $min
     * @param $max
     * @return mixed
     */
    public static function ensureNumberBetween($number, $min, $max)
    {
        if ($number > $max) {
            $number = $max;
        }

        if ($number < $min) {
            $number = $min;
        }

        return $number;
    }

    /**
     * @param string $str
     * @param string $default
     * @return string
     */
    public static function ensureStringFilled(string $str, string $default = ''): string
    {
        if ($str === ''){
            return $default;
        }
        return $str;
    }

    public static function compareArrays(array $initialArray, array $finishArray): array
    {
        $r = ['added' => [], 'deleted' => []];

        $arr1 = array_unique($initialArray, SORT_REGULAR);
        $arr2 = array_unique($finishArray, SORT_REGULAR);

        foreach ($arr1 as $value) {
            if (!in_array($value, $arr2, true)) {
                $r['deleted'][] = $value;
            }
        }

        foreach ($arr2 as $value) {
            if (!in_array($value, $arr1, true)) {
                $r['added'][] = $value;
            }
        }

        return $r;
    }
}