<?php

namespace Lkt\Factory;

/**
 * Class ColumnType
 * @package Lkt\Factory
 * @deprecated
 */
class ColumnType
{
    const STRING = 0;
    const INTEGER = 1;
    const FLOAT = 2;
    const BOOLEAN = 3;
    const HTML = 4;
    const EMAIL = 5;
    const FOREIGN_KEY = 6;

    const DATETIME = 7;
    const UNIX_TIMESTAMP = 8;

    const IMAGE = 9;
    const FILE = 10;

    const JSON = 11;
    const PIVOT = 12;
    const RELATED = 13;

    const COLOR = 14;

    const FOREIGN_KEYS = 15;
    const RELATED_KEYS = 16;
}