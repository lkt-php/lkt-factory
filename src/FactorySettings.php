<?php


namespace Lkt\Factory;

use Lkt\Drivers\ConnectionInterface;
use Lkt\Factory\AbstractInstances\AbstractInstance;

/**
 * Class FactorySettings
 * @package Lkt\Factory
 * @deprecated
 */
class FactorySettings extends \Lkt\Factory\Settings\FactorySettings
{

}