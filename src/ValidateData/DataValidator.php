<?php

namespace Lkt\Factory\ValidateData;

use Lkt\Factory\Cache\ColumnTypeController;
use Lkt\Factory\Cache\ColumnValidateVariables;
use Lkt\Factory\Cache\ColumnValidationController;
use Lkt\Factory\FactorySettings;
use Lkt\Factory\Parsers\DataParser;
use Lkt\InstancePatterns\AbstractInstances\AbstractAutomaticCheckerInstance;
use Lkt\InstancePatterns\Traits\InstantiableTrait;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;

/**
 * Class DataValidator
 * @package Lkt\Factory\ValidateData
 */
class DataValidator extends AbstractAutomaticCheckerInstance
{
    use InstantiableTrait;

    protected static $RESULT = [];

    protected $data = [];
    protected $type;

    /**
     * DataValidator constructor.
     * @param $type
     * @param $data
     */
    public function __construct($type, $data)
    {
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function handle(): bool
    {
        $parsed = [];
        $states = [];

        foreach ($this->data as $column => $datum) {
            $validateKey = ColumnValidateVariables::getInstance($column);
            $type = ColumnTypeController::getInstance($this->type, $column);
            $validations = ColumnValidationController::getInstance($this->type, $column);
            $isValid = true;

            $parsedValue = DataParser::parse($datum, $column, $this->type);
            foreach ($validations as $validationArgs) {
                $tempArgs = $validationArgs;
                if (!is_array($tempArgs)){
                    $isValid = $isValid && ($parsedValue !== null);
                    continue;
                }
                $args = array_merge([$parsedValue], $tempArgs);
                $validation = $tempArgs[0];
                unset($tempArgs[0]);
                $tempArgs = array_values($tempArgs);
                $isValid = $isValid && $validation::getInstance(...$args);
            }

            if ($type === COLUMN_FOREIGN_KEY) {
                $parsed[$column . 'Id'] = $parsedValue;
                $states[$validateKey . 'Id'] = $isValid;

            } else {
                $parsed[$column] = $parsedValue;
                if ($type !== COLUMN_BOOLEAN){
                    $states[$validateKey] = $isValid;
                }
            }
        }

        // First, execute custom data parser
        foreach ($this->data as $column => $datum) {
            $fields = FactorySettings::getComponentFields($this->type);
            $field = $fields[$column];
            $parser = $field['parser'];
            if ($parser){
                $parser::getInstance($parsed, $datum, $column, $field);
            }
        }


        static::$RESULT = $parsed + $states;
        return true;
    }

    public static function getResult()
    {
        return static::$RESULT;
    }
}