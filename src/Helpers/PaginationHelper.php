<?php


namespace Lkt\Factory\Helpers;

/**
 * Class PaginationHelper
 *
 * @package Lkt\Factory\Helpers
 * @deprecated
 */
class PaginationHelper
{
    /**
     * @param int $amountOfItems
     * @param int $itemsPerPage
     * @return int
     * @deprecated
     */
    public static function getTotalPages(int $amountOfItems, int $itemsPerPage): int
    {
        return (int)ceil($amountOfItems / $itemsPerPage);
    }
}