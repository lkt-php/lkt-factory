<?php


namespace Lkt\Factory\Parsers;

use Carbon\Carbon;
use chillerlan\Filereader\Directory;
use chillerlan\Filereader\Drivers\DiskDriver;
use chillerlan\Filereader\File;
use Lkt\Factory\Settings\FactorySettings;
use function Lkt\Factory\unescapeDatabaseCharacters;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_COLOR;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_EMAIL;
use const Lkt\Factory\COLUMN_FILE;
use const Lkt\Factory\COLUMN_FLOAT;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;
use const Lkt\Factory\COLUMN_FOREIGN_KEYS;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_IMAGE;
use const Lkt\Factory\COLUMN_INTEGER;
use const Lkt\Factory\COLUMN_JSON;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;


class DataParser
{
    protected static $DISK_DRIVER;
    protected static $DECIMAL_NUMBER_FORMATTER;

    /**
     * @return \NumberFormatter
     */
    public static function getDecimalNumberFormatter()
    {
        if (!is_object(static::$DECIMAL_NUMBER_FORMATTER)) {
            static::$DECIMAL_NUMBER_FORMATTER = new \NumberFormatter(FactorySettings::getLocale(), \NumberFormatter::DECIMAL);
        }
        return static::$DECIMAL_NUMBER_FORMATTER;
    }

    /**
     * @param $value
     * @param $column
     * @param $type
     * @return float|int|string|null
     */
    public static function parse($value, $column, $type)
    {
        $fields = FactorySettings::getComponentFields($type);
        $field = $fields[$column];
        $fieldType = $field['type'];

        switch ($fieldType) {
            case COLUMN_HTML:
                return unescapeDatabaseCharacters($value);

            case COLUMN_STRING:
            case COLUMN_EMAIL:
            case COLUMN_COLOR:
            case COLUMN_FOREIGN_KEYS:
                return trim($value);

            case COLUMN_BOOLEAN:
                $value = (int)$value;
                return $value === 1;

            case COLUMN_INTEGER:
            case COLUMN_FOREIGN_KEY:
                return (int)$value;

            case COLUMN_FLOAT:
                return (float)$value;

            case COLUMN_UNIX_TIMESTAMP:
                if (is_string($value)) {
                    $str = trim($value);
                } else {
                    $str = date('Y-m-d H:i:s', (int)$value);
                }
                if ($str === '') {
                    return null;
                }

                try {
                    $date = new \DateTime($str);
                    unset($date);
                    return new Carbon($str);

                } catch (\Exception $e) {
                    return null;
                }

            case COLUMN_DATETIME:
                $value = trim($value);
                if ($value === '') {
                    return null;
                }

                try {
                    $date = new \DateTime($value);
                    unset($date);
                    return new Carbon($value);

                } catch (\Exception $e) {
                    return null;
                }

            case COLUMN_FILE:
            case COLUMN_IMAGE:
                $value = trim($value);
                if ($value === '') {
                    return null;
                }

                if (!is_object(static::$DISK_DRIVER)) {
                    static::$DISK_DRIVER = new DiskDriver();
                }
                $directory = new Directory(static::$DISK_DRIVER, $field['path']);
                return new File(static::$DISK_DRIVER, $directory, $value);

            case COLUMN_JSON:
                if (is_string($value)){
                    $value = htmlspecialchars_decode($value);
                    $value = unescapeDatabaseCharacters($value);
                    $value = json_decode($value, true);
                } elseif (is_object($value)){
                    $value = json_decode(json_encode($value), true);
                } elseif (!is_array($value)){
                    return null;
                }
                return $value;

            default:
                return null;
        }
    }
}