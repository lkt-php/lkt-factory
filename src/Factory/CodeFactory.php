<?php

namespace Lkt\Factory\Factory;

use Lkt\Factory\AbstractInstances\AbstractInstance;
use Lkt\Factory\AbstractInstances\AbstractPivot;
use Lkt\Factory\Settings\FactorySettings;
use Lkt\InstancePatterns\AbstractInstances\AbstractAutomaticHandlerInstance;
use Lkt\InstancePatterns\Traits\InstantiableTrait;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_COLOR;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_EMAIL;
use const Lkt\Factory\COLUMN_FILE;
use const Lkt\Factory\COLUMN_FLOAT;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;
use const Lkt\Factory\COLUMN_FOREIGN_KEYS;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_IMAGE;
use const Lkt\Factory\COLUMN_INTEGER;
use const Lkt\Factory\COLUMN_JSON;
use const Lkt\Factory\COLUMN_PIVOT;
use const Lkt\Factory\COLUMN_RELATED;
use const Lkt\Factory\COLUMN_RELATED_KEYS;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;


class CodeFactory extends AbstractAutomaticHandlerInstance
{
    use InstantiableTrait;
    private static $FILE_TEMPLATE = "<?php namespace {NAMESPACE}; class {CLASS} extends {EXTENDS} {IMPLEMENTS} { {TRAITS} {METHODS} } ";

    /**
     * @return int
     */
    public function handle(): int
    {
        foreach (FactorySettings::getComponentCodes() as $code) {

            $schema = FactorySettings::getSchema($code);

            /**
             * Extended class
             */
            $extends = $schema->isPivot() ? AbstractPivot::class : AbstractInstance::class;
            $schemaExtends = $schema->getExtendedClassName();

            if ($schemaExtends !== '') {
                $extends = $schemaExtends;
            }

            /**
             * Class
             */
            $className = $schema->getClassName();
            $returnSelf = '\\' . $className;

            /**
             * Generated class
             */
            $generatedClassName = $schema->getGeneratedClassName();

            /**
             * Implemented interfaces
             */
            $implements = '';
            $schemaImplements = $schema->getImplementedInterfaces();

            if (isset($schemaImplements[0])) {
                $implements = '\\' . implode(',\\', $schemaImplements);
            }

            if ($implements !== ''){
                $implements = "implements {$implements}";
            }

            /**
             * Used traits
             */
            $traits = '';
            $schemaTraits = $schema->getUsedTraits();

            if (isset($schemaTraits[0])) {
                $traits = '\\' . implode(',\\', $schemaTraits);
            }

            if ($traits !== ''){
                $traits = "use {$traits}";
            }

            /**
             * Fields
             */
            $fields = $schema->getAllFields();
            $methods = ["const GENERATED_TYPE = '{$code}'; "];
            $methods[] = "public function __construct(\$id = 0) { parent::__construct(\$id, static::GENERATED_TYPE); }";

            foreach ($fields as $field => $fieldConfig) {
                $fieldName = ucfirst($field);
                $softTyped = $fieldConfig['softTyped'] === true;

                switch ($fieldConfig['type']) {
                    case COLUMN_STRING:
                    case COLUMN_HTML:
                        $methods[] = "public function get{$fieldName}() :string { return \$this->_getStringVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasStringVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(string \${$field}) { \$this->_setStringVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_BOOLEAN:
                        $methods[] = "public function {$field}() :bool { return \$this->_getBooleanVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(bool \${$field}) { \$this->_setBooleanVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_INTEGER:
                        $methods[] = "public function get{$fieldName}() :int { return \$this->_getIntegerVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasIntegerVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(int \${$field})  { \$this->_setIntegerVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_DATETIME:
                    case COLUMN_UNIX_TIMESTAMP:
                        $methods[] = "public function get{$fieldName}() { return \$this->_getDateTimeVal('{$field}'); }";
                        $methods[] = "public function get{$fieldName}Formatted(string \$format = null) :string { return \$this->_getDateTimeFormattedVal('{$field}', \$format); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasDateTimeVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(\${$field})  { \$this->_setDateTimeVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_FLOAT:
                        $methods[] = "public function get{$fieldName}() :float { return \$this->_getFloatVal('{$field}'); }";
                        $methods[] = "public function get{$fieldName}Formatted() :string { return \$this->_getFloatFormattedVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasFloatVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(float \${$field})  { \$this->_setFloatVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_FOREIGN_KEYS:
                        $methods[] = "public function get{$fieldName}() :string { return \$this->_getForeignListVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasForeignListVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(\${$field}) { \$this->_setForeignListVal('{$field}', \${$field}); return \$this; }";
                        if ($fieldConfig['component']) {
                            $classDoc = '@return :?\\'.FactorySettings::getComponentClassName($fieldConfig['component']) . '[]';
                            if ($softTyped){
                                $class = '';
                                $classDoc = '';
                            }
                            $methods[] = "/** {$classDoc} */public function get{$fieldName}Data(): array { return \$this->_getForeignListData('{$field}'); }";
                            $methods[] = "/** {$classDoc} */public function get{$fieldName}Ids(): array { return \$this->_getForeignListIds('{$field}'); }";
                        }
                        break;

                    case COLUMN_FOREIGN_KEY:
                        $methods[] = "public function get{$fieldName}Id() :int { return \$this->_getIntegerVal('{$field}Id'); }";
                        $methods[] = "public function has{$fieldName}Id() :bool { return \$this->_hasIntegerVal('{$field}Id'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}Id(int \${$field}Id)  { \$this->_setIntegerVal('{$field}Id', \${$field}Id); return \$this; }";
                        if ($fieldConfig['component']) {
                            $class = ':?\\'.FactorySettings::getComponentClassName($fieldConfig['component']);
                            $classDoc = '@return :?\\'.FactorySettings::getComponentClassName($fieldConfig['component']);
                            if ($softTyped){
                                $class = '';
                                $classDoc = '';
                            }
                            $methods[] = "/** {$classDoc} */public function get{$fieldName}(){$class} { return \$this->_getForeignVal('{$fieldConfig['component']}', \$this->get{$fieldName}Id()); }";
                            $methods[] = "public function has{$fieldName}() { return \$this->_hasForeignVal('{$fieldConfig['component']}', \$this->get{$fieldName}Id()); }";
                        }
                        break;

                    case COLUMN_FILE:
                    case COLUMN_IMAGE:
                        $methods[] = "public function get{$fieldName}() { return \$this->_getFileVal('{$field}'); }";
                        $methods[] = "public function get{$fieldName}InternalPath() { return \$this->_getInternalPath('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasFileVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(string \${$field}) { \$this->_setFileVal('{$field}', \${$field}); return \$this; }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}InternalPath(string \${$field}, string \$fileSource) { \$this->_setInternalPath('{$field}', \$fileSource); return \$this; }";

                        if ($fieldConfig['public']) {
                            $methods[] = "public function get{$fieldName}PublicPath() { return '{$fieldConfig['public']}/' . \$this->_getFileVal('{$field}')->name; }";
                        }
                        break;

                    case COLUMN_RELATED:
                        $class = 'array';
                        if ($fieldConfig['component']) {
                            $class = '\\' . FactorySettings::getComponentClassName($fieldConfig['component']) . '[]';
                        }
                        $methods[] = "/** @return {$class} */public function get{$fieldName}(bool \$forceRefresh = false) :array { return \$this->_getRelatedVal('{$fieldConfig['component']}', '{$field}', \$forceRefresh); }";
                        $methods[] = "/** @return \\Lkt\\Factory\\InstanceFactory */public function get{$fieldName}InstanceFactory() { return \$this->_getRelatedInstanceFactory('{$fieldConfig['component']}', '{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasRelatedVal('{$fieldConfig['component']}', '{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */ public function set{$fieldName}WithData(array \$data) { return \$this->_setRelatedValWithData('{$fieldConfig['component']}', '{$field}', \$data); }";
                        break;

                    case COLUMN_RELATED_KEYS:
                        $class = 'array';
                        if ($fieldConfig['component']) {
                            $class = '\\' . FactorySettings::getComponentClassName($fieldConfig['component']) . '[]';
                        }
                        $methods[] = "/** @return {$class} */public function get{$fieldName}() :array { return \$this->_getRelatedKeysVal('{$fieldConfig['component']}', '{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasRelatedKeysVal('{$fieldConfig['component']}', '{$field}'); }";
//                        $methods[] = "/** @return {$returnSelf} */ public function set{$fieldName}WithData(array \$data) { return \$this->_setRelatedKeysValWithData('{$fieldConfig['component']}', '{$field}', \$data); }";
                        break;

                    case COLUMN_PIVOT:
                        $class = 'array';
                        if ($fieldConfig['pivot']) {
                            $class = '\\' . FactorySettings::getComponentClassName($fieldConfig['component']) . '[]';
                        }
                        $methods[] = "/** @return {$class} */public function get{$fieldName}() :array { return \$this->_getPivotVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasPivotVal('{$field}'); }";
                        break;

                    case COLUMN_EMAIL:
                        $methods[] = "public function get{$fieldName}() :string { return \$this->_getEmailVal('{$field}'); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasEmailVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(string \${$field}) { \$this->_setEmailVal('{$field}', \${$field}); return \$this; }";
                        break;

                    case COLUMN_COLOR:
                        $methods[] = "public function get{$fieldName}() :string { return \$this->_getColorVal('{$field}'); }";
                        $methods[] = "public function get{$fieldName}Rgb(float \$opacity = null) :?array { return \$this->_getColorRgbVal('{$field}', \$opacity); }";
                        $methods[] = "public function get{$fieldName}RgbFormatted(float \$opacity = null) :string { return \$this->_getColorRgbStringVal('{$field}', \$opacity); }";
                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasColorVal('{$field}'); }";
                        $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(\$value = null)  { \$this->_setColorVal('{$field}', \$value); return \$this; }";
                        break;

                    case COLUMN_JSON:
                        if ($fieldConfig['assoc']) {
                            $methods[] = "public function get{$fieldName}() :?array { return \$this->_getJsonVal('{$field}'); }";
                            $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(array \${$field}) { \$this->_setJsonVal('{$field}', \${$field}); return \$this; }";
                        } else {
                            $methods[] = "public function get{$fieldName}() :?\StdClass { return \$this->_getJsonVal('{$field}'); }";
                            $methods[] = "/** @return {$returnSelf} */public function set{$fieldName}(\StdClass \${$field}) { \$this->_setJsonVal('{$field}', \${$field}); return \$this; }";
                        }

                        $methods[] = "public function has{$fieldName}() :bool { return \$this->_hasJsonVal('{$field}'); }";
                        break;
                }
            }


            $extends = '\\'.$extends;
            $fileContent = str_replace([
                '{NAMESPACE}',
                '{CLASS}',
                '{EXTENDS}',
                '{METHODS}',
                '{IMPLEMENTS}',
                '{TRAITS}',
            ], [
                $schema->getNamespace(),
                $generatedClassName,
                $extends,
                implode('', $methods),
                $implements,
                $traits,
            ], static::$FILE_TEMPLATE);

            $filePath = $schema->getGeneratedClassFullPath();
            $status = file_put_contents($filePath, $fileContent);
        }

        return 1;
    }
}