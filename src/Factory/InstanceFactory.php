<?php

namespace Lkt\Factory\Factory;

use Lkt\Factory\Builders\WhereBuilder;
use Lkt\Factory\Cache\FactoryCache;
use Lkt\Factory\LoadData\InstanceGenerator;
use Lkt\Factory\LoadData\QueryBuilder;
use Lkt\Factory\LoadData\QueryColumnController;
use Lkt\Factory\Settings\FactorySettings;
use Lkt\Factory\ValidateData\DataValidator;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class InstanceFactory
 * @package Lkt\Factory
 */
class InstanceFactory
{
    use InstantiableTrait;

    protected $type;
    protected $id;
    protected $forceRefresh = false;
    protected $page = 0;
    protected $limit = 0;
    protected $where = '';
    protected $orderBy = '';
    protected $queryBuilder;
    protected $oldQueryBuilderMode = false;

    /**
     * InstanceFactory constructor.
     * @param $type
     * @param int $id
     */
    public function __construct($type, $id = 0)
    {
        $this->type = $type;
        $this->id = $id;
        $this->queryBuilder = QueryBuilder::getInstance();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return $this
     */
    public function pagination(int $page = 0, int $limit = 0): self
    {
        if ($page < 1) {
            $page = 1;
        }
        --$page;
        $this->page = $page;
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param string $where
     * @return $this
     */
    public function where(string $where = ''): self
    {
        $this->where = trim($where);
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function forceRefresh($status = true): self
    {
        $this->forceRefresh = $status;
        return $this;
    }

    /**
     * @param string $where
     * @return string
     */
    public function getWhere(string $where = ''): string
    {
        return $this->where;
    }

    /**
     * @param string $orderBy
     * @return $this
     */
    public function orderBy(string $orderBy = '') :self
    {
        $this->orderBy = trim($orderBy);
        return $this;
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        $this->oldQueryBuilderMode = true;
        $table = FactorySettings::getComponentTable($this->type);
        $queryBuilder = $this->queryBuilder;
        $idColumn = FactorySettings::getComponentIdColumn($this->type);
        $idColumnConfig = FactorySettings::getComponentFields($this->type)[$idColumn];

        $queryBuilder->setModule($this->type);
        if ($queryBuilder->from === ''){
            $queryBuilder->setFrom($table);
        }

        if ($this->limit > 0) {
            $queryBuilder->setPage($this->page);
            $queryBuilder->setLimit($this->limit);
        }

        if ($this->where !== '') {
            $queryBuilder->setWhere($this->where);
        }

        if ($this->id > 0) {
            $queryBuilder
                ->setWhere(implode(' AND ', [$queryBuilder->where, "{$table}.{$idColumnConfig['column']} = {$this->id}"]))
                ->setLimit(1)
            ;
        }

        $queryBuilder->setForceRefresh($this->forceRefresh);

        if ($this->orderBy !== '') {
            $queryBuilder->setOrderBy($this->orderBy);
        }

        return $queryBuilder;
    }

    /**
     * @return \Lkt\Factory\Builders\QueryBuilder
     */
    public function getQueryBuilder2(): \Lkt\Factory\Builders\QueryBuilder
    {
        $table = FactorySettings::getComponentTable($this->type);
        $builder = \Lkt\Factory\Builders\QueryBuilder::table($table, $this->type)
            ->setColumns(explode(',', QueryColumnController::getInstance($this->type)))
        ;

        if ($this->page > -1 && $this->limit > 0) {
            $builder->pagination(($this->page + 1), $this->limit);
        }
        $idColumn = FactorySettings::getComponentIdColumn($this->type);
        $idColumnConfig = FactorySettings::getComponentFields($this->type)[$idColumn];

        if ($this->where !== '') {
            $builder->constraint($this->where);
        }

        if ($this->id > 0) {
            $builder
                ->where(WhereBuilder::integerEqual("{$table}.{$idColumnConfig['column']}", $this->id))
                ->pagination(-1, 1);
        }

        if ($this->orderBy !== '') {
            $builder->orderBy($this->orderBy);
        }

        return $builder;
    }

    /**
     * @param string $countableColumn
     * @return int
     */
    public function count(string $countableColumn): int
    {
        if ($this->oldQueryBuilderMode) {
            return $this->getQueryBuilder()->getCount($countableColumn);
        } else {
            return $this->getQueryBuilder2()->count($countableColumn);
        }
    }

    public function query()
    {
        if ($this->oldQueryBuilderMode) {
            $results = $this->getQueryBuilder()->getResults();
        } else {
            $results = $this->getQueryBuilder2()->select();
        }

        $schema = FactorySettings::getSchema($this->type);
        $idColumn = $schema->getIdColumn();
        if (count($idColumn) === 1) {
            $idColumn = trim($idColumn[0]);
        }
        $r = [];

        foreach ($results as $item){
            if (is_array($idColumn)) {
                $id = [];
                foreach ($idColumn as $t) {
                    $id[] = $item[$t];
                }

                $id = implode('-', $id);
            } else {
                $id = (int)$item[$idColumn];
            }

            DataValidator::getInstance($this->type, $item);
            FactoryCache::store("{$this->type}_{$id}", DataValidator::getResult());
            $r[] = InstanceGenerator::getInstance($this->type, $id, $this->forceRefresh);
        }
        return $r;
    }

    /**
     * @return array
     * @deprecated
     */
    public function load()
    {
        return $this->query();
    }

    public function instance()
    {
        if ($this->id > 0){
            return InstanceGenerator::getInstance($this->type, $this->id);
        }
        $results = $this->query();
        if (isset($results[0])){
            return $results[0];
        }
        return InstanceGenerator::getInstance($this->type, $this->id);
    }
}