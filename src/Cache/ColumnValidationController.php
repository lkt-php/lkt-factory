<?php

namespace Lkt\Factory\Cache;

use Lkt\Factory\FactorySettings;
use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;
use Lkt\Validations\DatetimeValidations\IsValidCarbonObjectValidation;
use Lkt\Validations\NumericValidations\NumberGreaterThanValidation;
use Lkt\Validations\StringValidations\HtmlNotEmptyValidation;
use Lkt\Validations\StringValidations\StringNotEmptyValidation;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;

/**
 * Class ColumnTypeController
 * @package Lkt\Factory\Cache
 */
class ColumnValidationController implements CacheControllerInterface
{
    use InstantiableTrait,
        CacheControllerTrait,
        AutomaticInstanceTrait;

    protected $type;
    protected $typeCode;
    protected $column;

    /**
     * ColumnTypeController constructor.
     * @param $type
     * @param $column
     */
    public function __construct($type, $column)
    {
        $this->type = $type;
        $this->column = $column;
        $this->typeCode = trim("{$this->type}_{$this->column}");
    }

    /**
     * @return string
     */
    public function handle()
    {
        if (static::inCache($this->typeCode)) {
            return static::load($this->typeCode);
        }
        $fields = FactorySettings::getComponentFields($this->type);
        $type = ColumnTypeController::getInstance($this->type, $this->column);
        $validations = $fields[$this->column]['validations'];
        if (!is_array($validations)) {
            $validations = [];
        }

        switch ($type) {
            case COLUMN_STRING:
                $validations[] = [StringNotEmptyValidation::class];
                break;

            case COLUMN_HTML:
                $validations[] = [StringNotEmptyValidation::class];
                $validations[] = [HtmlNotEmptyValidation::class];
                break;

            case COLUMN_FOREIGN_KEY:
                $validations[] = [NumberGreaterThanValidation::class, 0];
                break;

            case COLUMN_DATETIME:
            case COLUMN_UNIX_TIMESTAMP:
                $validations[] = [IsValidCarbonObjectValidation::class];
        }
        static::store($this->typeCode, $validations);
        return $validations;
    }
}