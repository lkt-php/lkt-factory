<?php

namespace Lkt\Factory\Cache;

use Lkt\Factory\FactorySettings;
use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;


class ColumnTypeController implements CacheControllerInterface
{
    use InstantiableTrait,
        CacheControllerTrait,
        AutomaticInstanceTrait;

    protected $type;
    protected $typeCode;
    protected $column;

    /**
     * ColumnTypeController constructor.
     * @param $type
     * @param $column
     */
    public function __construct($type, $column)
    {
        $this->type = $type;
        $this->column = $column;
        $this->typeCode = trim("{$this->type}_{$this->column}");
    }

    /**
     * @return string
     */
    public function handle()
    {
        if (static::inCache($this->typeCode)) {
            return static::load($this->typeCode);
        }
        $fields = FactorySettings::getComponentFields($this->type);
        $type = (int)$fields[$this->column]['type'];
        static::store($this->typeCode, $type);
        return $type;
    }
}