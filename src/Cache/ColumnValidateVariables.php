<?php

namespace Lkt\Factory\Cache;

use Lkt\InstancePatterns\AbstractInstances\AbstractAutomaticParserInstance;
use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class ColumnValidateVariables
 * @package Lkt\Factory\Cache
 */
class ColumnValidateVariables extends AbstractAutomaticParserInstance implements CacheControllerInterface
{
    use CacheControllerTrait,
        InstantiableTrait;

    protected $column;

    /**
     * ColumnValidateVariables constructor.
     * @param string $column
     */
    public function __construct(string $column)
    {
        $this->column = $column;
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        if (static::inCache($this->column)) {
            return static::load($this->column);
        }

        $r = trim('has'.ucfirst($this->column));
        static::store($this->column, $r);
        return $r;
    }
}