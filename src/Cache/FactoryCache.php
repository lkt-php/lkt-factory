<?php

namespace Lkt\Factory\Cache;

use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;

/**
 * Class FactoryCache
 * @package Lkt\Factory\Cache
 */
class FactoryCache implements CacheControllerInterface
{
    use CacheControllerTrait;
}