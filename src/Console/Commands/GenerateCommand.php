<?php

namespace Lkt\Factory\Console\Commands;

use Lkt\Factory\Factory\CodeFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GenerateCommand extends Command
{
    protected static $defaultName = 'lkt:factory:generate';

    public function execute(InputInterface $input, OutputInterface $output)
    {
        CodeFactory::getInstance();
        return 1;
    }
}