<?php

namespace Lkt\Factory\Settings;

use function Lkt\Tools\Parse\ensureStringFilled;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_COLOR;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_EMAIL;
use const Lkt\Factory\COLUMN_FILE;
use const Lkt\Factory\COLUMN_FLOAT;
use const Lkt\Factory\COLUMN_FOREIGN_KEYS;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_IMAGE;
use const Lkt\Factory\COLUMN_JSON;
use const Lkt\Factory\COLUMN_PIVOT;
use const Lkt\Factory\COLUMN_RELATED;
use const Lkt\Factory\COLUMN_RELATED_KEYS;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;

class Schema implements SchemaInterface
{
    use DbSettingsTrait;

    protected $composition = [];

    public function composeWith(string $component, string $fieldConfig): self
    {
        $this->composition[$component] = $fieldConfig;
        return $this;
    }

    /**
     * @param string $table
     * @param string $component
     */
    public function __construct(string $table, string $component)
    {
        $this->table = $table;
        $this->component = $component;
        $this->setIdField();
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setIdField(string $field = 'id', string $dbCol = 'id'): self
    {
        $this->idColumn = [$field];
        return $this->setIntegerField($field, $dbCol);
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setFloatField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_FLOAT,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setStringField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_STRING,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setColorField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_COLOR,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setHTMLField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_HTML,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setEmailField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_EMAIL,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setBooleanField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_BOOLEAN,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setDatetimeField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_DATETIME,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setUnixTimestampField(string $field, string $dbCol = ''): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_UNIX_TIMESTAMP,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param bool $assoc
     * @return $this
     */
    public function setJSONField(string $field, string $dbCol = '', bool $assoc = true, bool $compress = false): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_JSON,
            'column' => $dbCol,
            'assoc' => $assoc,
            'compress' => $compress,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param array $orderBy
     * @return $this
     */
    public function setRelatedField(string $field, string $dbCol = '', string $component = '', array $constraints = [], array $orderBy = []): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_RELATED,
            'column' => $dbCol,
            'component' => $component,
            'where' => $constraints,
            'order' => $orderBy
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param array $orderBy
     * @param bool $softTyped
     * @return $this
     */
    public function setForeignKeysField(string $field, string $dbCol = '', string $component = '', array $constraints = [], array $orderBy = [], bool $softTyped = false): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_FOREIGN_KEYS,
            'column' => $dbCol,
            'component' => $component,
            'where' => $constraints,
            'softTyped' => $softTyped,
            'order' => $orderBy
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param array $orderBy
     * @param bool $softTyped
     * @return $this
     */
    public function setRelatedKeysField(string $field, string $dbCol = '', string $component = '', array $constraints = [], array $orderBy = [], bool $softTyped = false): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_RELATED_KEYS,
            'column' => $dbCol,
            'component' => $component,
            'where' => $constraints,
            'softTyped' => $softTyped,
            'order' => $orderBy
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $component
     * @param string $pivot
     * @param array $constraints
     * @param array $orderBy
     * @return $this
     */
    public function setPivotField(string $field, string $component = '', string $pivot = '', array $constraints = [], array $orderBy = []): self
    {
        $this->fields[$field] = [
            'type' => COLUMN_PIVOT,
            'component' => $component,
            'pivot' => $pivot,
            'where' => $constraints,
            'order' => $orderBy
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $storePath
     * @param string $publicPath
     * @return $this
     */
    public function setFileField(string $field, string $dbCol, string $storePath, string $publicPath): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_FILE,
            'column' => $dbCol,
            'path' => $storePath,
            'public' => $publicPath,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $storePath
     * @param string $publicPath
     * @return $this
     */
    public function setImageField(string $field, string $dbCol, string $storePath, string $publicPath): self
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_IMAGE,
            'column' => $dbCol,
            'path' => $storePath,
            'public' => $publicPath,
        ];
        return $this;
    }
}