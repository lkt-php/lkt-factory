<?php

namespace Lkt\Factory\Settings;

class Pivot implements SchemaInterface
{
    use DbSettingsTrait;

    /**
     * @param string $table
     * @param string $component
     */
    public function __construct(string $table, string $component)
    {
        $this->table = $table;
        $this->component = $component;
        $this->pivot = true;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param bool $softTyped
     * @return $this
     */
    public function setLeftIdField(string $field, string $dbCol = '', string $component = '', array $constraints = [], bool $softTyped = false): self
    {
        $this->idColumn[0] = $field;
        return $this->setForeignKeyField($field, $dbCol, $component, $constraints, $softTyped);
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param bool $softTyped
     * @return $this
     */
    public function setRightIdField(string $field, string $dbCol = '', string $component = '', array $constraints = [], bool $softTyped = false): self
    {
        $this->idColumn[1] = $field;
        return $this->setForeignKeyField($field, $dbCol, $component, $constraints, $softTyped);
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return $this
     */
    public function setPositionField(string $field, string $dbCol = ''): self
    {
        return $this->setIntegerField($field, $dbCol);
    }
}