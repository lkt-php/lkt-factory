<?php

namespace Lkt\Factory\Settings;

interface SchemaInterface
{
    public static function table(string $table, string $component): self;

    /**
     * @param string $namespace
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setNamespace(string $namespace): self;

    /**
     * @param string $interface
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setInterface(string $interface): self;

    /**
     * @param string $trait
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setTrait(string $trait): self;

    /**
     * @param string $name
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setNameForGeneratedClass(string $name): self;

    /**
     * @param string $path
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setWhereStoreGeneratedClass(string $path): self;

    /**
     * @param string $className
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setClassToBeExtended(string $className): self;

    /**
     * @param string $className
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setAppClass(string $className): self;

    /**
     * @param string $component
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setBaseComponent(string $component): self;

    /**
     * @param string $field
     * @param string $dbCol
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setIntegerField(string $field, string $dbCol = ''): self;

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param bool $softTyped
     * @return Pivot|DbSettingsTrait|Schema
     */
    public function setForeignKeyField(string $field, string $dbCol = '', string $component = '', array $constraints = [], bool $softTyped = false): self;

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @param array $data
     * @param string $component
     * @return DbSettingsTrait|Pivot|Schema
     */
    public static function fromArray(array $data, string $component);

    /**
     * @return bool
     */
    public function isPivot(): bool;
    /**
     * @return string
     */
    public function getExtendedClassName(): string;

    /**
     * @return string
     */
    public function getClassName(): string;

    /**
     * @return string
     */
    public function getGeneratedClassName(): string;

    /**
     * @return string
     */
    public function getNamespace(): string;

    /**
     * @return string
     */
    public function getTable(): string;

    /**
     * @return array
     */
    public function getImplementedInterfaces(): array;

    /**
     * @return array
     */
    public function getUsedTraits(): array;

    /**
     * @return string
     */
    public function getGeneratedClassFullPath(): string;

    /**
     * @return array
     */
    public function getFields(): array;

    /**
     * @return array
     */
    public function getAllFields(): array;

    /**
     * @param string $field
     * @return array
     */
    public function getField(string $field): array;

    /**
     * @param bool $asString
     * @return string|string[]
     */
    public function getIdColumn(bool $asString = false);

    /**
     * @return string
     */
    public function getComponent(): string;
}