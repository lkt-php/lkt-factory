<?php

namespace Lkt\Factory\Settings;

use function Lkt\Tools\Parse\ensureStringFilled;
use const Lkt\Factory\COLUMN_BOOLEAN;
use const Lkt\Factory\COLUMN_COLOR;
use const Lkt\Factory\COLUMN_DATETIME;
use const Lkt\Factory\COLUMN_EMAIL;
use const Lkt\Factory\COLUMN_FILE;
use const Lkt\Factory\COLUMN_FLOAT;
use const Lkt\Factory\COLUMN_FOREIGN_KEY;
use const Lkt\Factory\COLUMN_FOREIGN_KEYS;
use const Lkt\Factory\COLUMN_HTML;
use const Lkt\Factory\COLUMN_IMAGE;
use const Lkt\Factory\COLUMN_INTEGER;
use const Lkt\Factory\COLUMN_JSON;
use const Lkt\Factory\COLUMN_PIVOT;
use const Lkt\Factory\COLUMN_RELATED;
use const Lkt\Factory\COLUMN_RELATED_KEYS;
use const Lkt\Factory\COLUMN_STRING;
use const Lkt\Factory\COLUMN_UNIX_TIMESTAMP;

trait DbSettingsTrait
{
    protected $pivot = false;
    protected $table = '';
    protected $component = '';
    protected $idColumn = ['id'];
    protected $fields = [];

    protected $namespace = '';
    protected $generatedClassName = '';
    protected $pathToStoreGeneratedClass = '';
    protected $extendsClass = '';
    protected $instantiateClass = '';
    protected $baseSchemaConfig = '';
    protected $implementsInterfaces = [];
    protected $traits = [];

    /**
     * @param string $table
     * @param string $component
     * @return SchemaInterface
     */
    public static function table(string $table, string $component): SchemaInterface
    {
        return new static($table, $component);
    }

    /**
     * @param string $namespace
     * @return SchemaInterface
     */
    public function setNamespace(string $namespace): SchemaInterface
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @param string $interface
     * @return SchemaInterface
     */
    public function setInterface(string $interface): SchemaInterface
    {
        $this->implementsInterfaces[] = $interface;
        return $this;
    }

    /**
     * @param string $trait
     * @return SchemaInterface
     */
    public function setTrait(string $trait): SchemaInterface
    {
        $this->traits[] = $trait;
        return $this;
    }

    /**
     * @param string $name
     * @return SchemaInterface
     */
    public function setNameForGeneratedClass(string $name): SchemaInterface
    {
        $this->generatedClassName = $name;
        return $this;
    }

    /**
     * @param string $path
     * @return SchemaInterface
     */
    public function setWhereStoreGeneratedClass(string $path): SchemaInterface
    {
        $this->pathToStoreGeneratedClass = $path;
        return $this;
    }

    /**
     * @param string $className
     * @return SchemaInterface
     */
    public function setClassToBeExtended(string $className): SchemaInterface
    {
        $this->extendsClass = $className;
        return $this;
    }

    /**
     * @param string $className
     * @return SchemaInterface
     */
    public function setAppClass(string $className): SchemaInterface
    {
        $this->instantiateClass = $className;
        return $this;
    }

    /**
     * @param string $component
     * @return SchemaInterface
     */
    public function setBaseComponent(string $component): SchemaInterface
    {
        $this->baseSchemaConfig = $component;
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @return SchemaInterface
     */
    public function setIntegerField(string $field, string $dbCol = ''): SchemaInterface
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_INTEGER,
            'column' => $dbCol,
        ];
        return $this;
    }

    /**
     * @param string $field
     * @param string $dbCol
     * @param string $component
     * @param array $constraints
     * @param bool $softTyped
     * @return SchemaInterface
     */
    public function setForeignKeyField(string $field, string $dbCol = '', string $component = '', array $constraints = [], bool $softTyped = false): SchemaInterface
    {
        $dbCol = ensureStringFilled($dbCol, $field);
        $this->fields[$field] = [
            'type' => COLUMN_FOREIGN_KEY,
            'column' => $dbCol,
            'component' => $component,
            'where' => $constraints,
            'softTyped' => $softTyped,
        ];
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'table' => $this->table,
            'idColumn' => $this->pivot ? $this->idColumn : $this->idColumn[0],
            'pivot' => $this->pivot,
            'instance' => [
                'namespace' => $this->namespace,
                'classname' => $this->generatedClassName,
                'storePath' => $this->pathToStoreGeneratedClass,
                'class' => $this->instantiateClass,
                'extends' => $this->extendsClass,
                'implements' => $this->implementsInterfaces,
                'traits' => $this->traits,
            ],
            'base' => $this->baseSchemaConfig,
            'fields' => $this->fields,
        ];
    }

    /**
     * @param array $data
     * @param string $component
     * @return SchemaInterface
     */
    public static function fromArray(array $data, string $component): SchemaInterface
    {
        if (!isset($data['pivot']) || !$data['pivot']) {
            $ins = Schema::table($data['table'], $component);
        } else {
            $ins = Pivot::table($data['table'], $component);
        }

        $instanceCfg = isset($data['instance']) ? $data['instance'] : [];
        $namespace = isset($instanceCfg['namespace']) ? trim($instanceCfg['namespace']) : '';
        $classname = isset($instanceCfg['classname']) ? trim($instanceCfg['classname']) : '';
        $storePath = isset($instanceCfg['storePath']) ? trim($instanceCfg['storePath']) : '';
        $class = isset($instanceCfg['class']) ? trim($instanceCfg['class']) : '';
        $extends = isset($instanceCfg['extends']) ? trim($instanceCfg['extends']) : '';
        $base = isset($data['base']) ? trim($data['base']) : '';

        $ins
            ->setNamespace($namespace)
            ->setNameForGeneratedClass($classname)
            ->setWhereStoreGeneratedClass($storePath)
            ->setAppClass($class)
            ->setClassToBeExtended($extends)
            ->setBaseComponent($base);

        $implements = '';
        if (isset($data['instance']) && isset($data['instance']['implements'])) {
            $implements = trim($data['instance']['implements']);
        }
        if ($implements !== '') {
            $ins->setInterface($implements);
        }

        if (isset($data['instance']['traits'])) {
            if (is_array($data['instance']['traits'])) {
                foreach ($data['instance']['traits'] as $trait) {
                    $ins->setTrait(trim($trait));
                }
            } elseif (is_string($data['instance']['traits'])) {
                $ins->setTrait(trim($data['instance']['traits']));
            }
        }

        if (!isset($data['pivot']) || !$data['pivot']) {
            $ins->setIdField($data['idColumn']);

            foreach ($data['fields'] as $field => $fieldConfig) {

                $where = isset($fieldConfig['where']) ? $fieldConfig['where'] : [];
                if (!$where) {
                    $where = [];
                }

                $order = isset($fieldConfig['order']) ? $fieldConfig['order'] : [];
                if (!$order) {
                    $order = [];
                }

                $compress = false;
                if (isset($fieldConfig['compress']) && $fieldConfig['compress'] === true) {
                    $compress = true;
                }

                $softTyped = false;
                if (isset($fieldConfig['softTyped']) && $fieldConfig['softTyped'] === true) {
                    $softTyped = true;
                }

                $assoc = false;
                if (isset($fieldConfig['assoc']) && $fieldConfig['assoc'] === true) {
                    $assoc = true;
                }

                switch ($fieldConfig['type']) {
                    case COLUMN_STRING:
                        $ins->setStringField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_EMAIL:
                        $ins->setEmailField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_HTML:
                        $ins->setHTMLField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_BOOLEAN:
                        $ins->setBooleanField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_INTEGER:
                        $ins->setIntegerField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_DATETIME:
                        $ins->setDatetimeField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_UNIX_TIMESTAMP:
                        $ins->setUnixTimestampField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_FLOAT:
                        $ins->setFloatField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_FOREIGN_KEYS:
                        $ins->setForeignKeysField($field, $fieldConfig['column'], $fieldConfig['component'], $where, $order, $softTyped);
                        break;

                    case COLUMN_FOREIGN_KEY:
                        $ins->setForeignKeyField($field, $fieldConfig['column'], $fieldConfig['component'], $where, $softTyped);
                        break;

                    case COLUMN_FILE:
                        $ins->setFileField($field, $fieldConfig['column'], trim($fieldConfig['storePath']), trim($fieldConfig['publicPath']));
                        break;

                    case COLUMN_IMAGE:
                        $ins->setImageField($field, $fieldConfig['column'], trim($fieldConfig['storePath']), trim($fieldConfig['publicPath']));
                        break;

                    case COLUMN_RELATED:
                        $ins->setRelatedField($field, $fieldConfig['column'], $fieldConfig['component'], $where, $order);
                        break;

                    case COLUMN_RELATED_KEYS:
                        $ins->setRelatedKeysField($field, $fieldConfig['column'], $fieldConfig['component'], $where, $order, $softTyped);
                        break;

                    case COLUMN_PIVOT:
                        $ins->setPivotField($field, $fieldConfig['component'], $fieldConfig['pivot'], $where, $order);
                        break;

                    case COLUMN_COLOR:
                        $ins->setColorField($field, $fieldConfig['column']);
                        break;

                    case COLUMN_JSON:
                        $ins->setJSONField($field, $fieldConfig['column'], $assoc, $compress);
                        break;
                }
            }
        } else {
            $leftField = $data['idColumn'][0];
            $leftFieldCnf = $data['fields'][$leftField];
            $ins->setLeftIdField($leftField, $leftFieldCnf['column'], $leftFieldCnf['component']);

            $rightField = $data['idColumn'][1];
            $rightFieldCnf = $data['fields'][$rightField];
            $ins->setRightIdField($rightField, $rightFieldCnf['column'], $rightFieldCnf['component']);

            $keys = array_keys($data['fields']);
            $positionField = $keys[2];
            $positionFieldCnf = $data['fields'][$positionField];
            $ins->setPositionField($positionField, $positionFieldCnf['column']);
        }

        return $ins;
    }

    /**
     * @return bool
     */
    public function isPivot(): bool
    {
        return $this->pivot === true;
    }

    /**
     * @return string
     */
    public function getExtendedClassName(): string
    {
        return $this->extendsClass;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->instantiateClass;
    }

    /**
     * @return string
     */
    public function getGeneratedClassName(): string
    {
        return $this->generatedClassName;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @return array
     */
    public function getImplementedInterfaces(): array
    {
        return $this->implementsInterfaces;
    }

    /**
     * @return array
     */
    public function getUsedTraits(): array
    {
        return $this->traits;
    }

    /**
     * @return string
     */
    public function getGeneratedClassFullPath(): string
    {
        return "{$this->pathToStoreGeneratedClass}/{$this->generatedClassName}.php";
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getAllFields(): array
    {
        $r = $this->fields;

        if ($this->extendsClass !== ''
            && class_exists($this->extendsClass)
            && defined("{$this->extendsClass}::GENERATED_TYPE")) {

            $code = $this->extendsClass::GENERATED_TYPE;
            if ($code) {
                $schema = FactorySettings::getSchema($code);
                $fields = $schema->getAllFields();
                foreach ($fields as $column => $field) {
                    if (!array_key_exists($column, $r)) {
                        $r[$column] = $field;
                    }
                }
            }
        }

        if ($this->baseSchemaConfig !== '') {
            $baseSchema = FactorySettings::getSchema($this->baseSchemaConfig);
            if ($baseSchema) {
                $fields = $baseSchema->getAllFields();
                foreach ($fields as $column => $field) {
                    if (!array_key_exists($column, $r)) {
                        $r[$column] = $field;
                    }
                }
            }
        }
        return $r;
    }

    /**
     * @param string $field
     * @return array
     */
    public function getField(string $field): array
    {
        $haystack = $this->getAllFields();
        if (isset($haystack[$field])) {
            return $haystack[$field];
        }
        return [];
    }

    /**
     * @param bool $asString
     * @return string|string[]
     */
    public function getIdColumn(bool $asString = false)
    {
        if ($asString) {
            return implode('-', $this->idColumn);
        }
        return $this->idColumn;
    }

    /**
     * @return string
     */
    public function getComponent(): string
    {
        return $this->component;
    }

    /**
     * @param string $component
     * @param bool $matchOne
     * @return array
     */
    public function getFieldsPointingToComponent(string $component, bool $matchOne = false): array
    {
        $results = array_filter($this->getAllFields(), function ($field) use ($component) {
            return $field['component'] === $component;
        });

        if ($matchOne) {
            $results = array_values($results);
            if (count($results) > 0) {
                $results = $results[0];
            }
        }
        return $results;
    }

    /**
     * @param string $component
     * @param bool $matchOne
     * @return array|string
     */
    public function getColumnsPointingToComponent(string $component, bool $matchOne = false)
    {
        $results = array_map(function ($item) {
            return $item['column'];
        }, array_filter($this->getAllFields(), function ($field) use ($component) {
            return $field['component'] === $component;
        }));

        if ($matchOne) {
            $results = array_values($results);
            if (count($results) > 0) {
                return $results[0];
            }
        }
        return array_values($results);
    }
}