<?php


namespace Lkt\Factory\Settings;

use Lkt\Drivers\ConnectionInterface;
use Lkt\Factory\AbstractInstances\AbstractInstance;


class FactorySettings
{
    protected static $CONNECTION;
    protected static $CONNECTIONS = [];
    public static $CONFIG_FILES = [];
    protected static $LOCALE = 'en_US';
    public static $schemas = [];

    public static function setLocale(string $locale)
    {
        static::$LOCALE = $locale;
    }

    public static function getLocale()
    {
        return static::$LOCALE;
    }

    /**
     * @param ConnectionInterface $connection
     */
    public static function setConnection(ConnectionInterface $connection)
    {
        static::$CONNECTION = $connection;
    }

    /**
     * @param ConnectionInterface $connection
     * @param string $name
     */
    public static function addConnection(ConnectionInterface $connection, string $name)
    {
        static::$CONNECTIONS[$name] = $connection;
    }

    public static function getConfigFile($code): array
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return [];
        }
        return $schema->toArray();
    }

    public static function loadSchema(Schema $schema)
    {
        $code = $schema->getComponent();
        static::$CONFIG_FILES[$code] = $schema->toArray();
        static::$schemas[$code] = $schema;
    }

    public static function loadPivot(Pivot $schema)
    {
        $code = $schema->getComponent();
        static::$CONFIG_FILES[$code] = $schema->toArray();
        static::$schemas[$code] = $schema;
    }

    /**
     * @param $code
     * @param string $path
     */
    public static function loadConfigFile($code, string $path)
    {
        $data = include $path;
        if (is_array($data)) {
            static::$CONFIG_FILES[$code] = $data;
            static::$schemas[$code] = Schema::fromArray($data, $code);

        } elseif ($data instanceof SchemaInterface) {
            static::$CONFIG_FILES[$code] = $data->toArray();
            static::$schemas[$code] = $data;
        }

        $instanceSettings = static::$CONFIG_FILES[$code]['instance'];
        if (isset($instanceSettings['extends'])) {
            if (class_exists($instanceSettings['extends'])) {
                $extendsClassName = $instanceSettings['extends'];
                $generatedType= '';
                if (defined("{$extendsClassName}::GENERATED_TYPE")) {
                    $generatedType = $extendsClassName::GENERATED_TYPE;
                }
                if ($generatedType) {
                    $fields = static::getComponentFields($generatedType);
                    foreach ($fields as $column => $field) {
                        if (!array_key_exists($column, static::$CONFIG_FILES[$code]['fields'])) {
                            static::$CONFIG_FILES[$code]['fields'][$column] = $field;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $code
     * @return string
     */
    public static function getComponentTable($code): string
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return '';
        }
        return $schema->getTable();
    }

    /**
     * @param $code
     * @return bool
     */
    public static function isPivot($code): bool
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return false;
        }
        return $schema->isPivot();
    }

    /**
     * @param $code
     * @return string
     */
    public static function getComponentIdColumn($code)
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return false;
        }

        return $schema->getIdColumn(true);
    }

    /**
     * @param $code
     * @return array
     */
    public static function getComponentFields($code): array
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return [];
        }
        return $schema->getAllFields();
    }

    /**
     * @param string $code
     * @param string $column
     * @return array
     */
    public static function getComponentField(string $code, string $column): array
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return [];
        }
        return $schema->getField($column);
    }

    /**
     * @param $code
     * @param $column
     * @return int|string
     */
    public static function getComponentFieldKey($code, $column)
    {
        $r = static::getComponentFields($code);
        foreach ($r as $k => $value) {
            if ($value['column'] === $column) {
                return $k;
            }
        }
        return '';
    }

    /**
     * @param $code
     * @param $componentCode
     * @return mixed|null
     */
    public static function getComponentFieldPointingToComponent($code, $componentCode)
    {
        $r = array_filter(static::getComponentFields($code), function ($field) use ($componentCode) {
            return $field['component'] === $componentCode;
        });

        if (count($r) > 0) {
            return array_values($r)[0];
        }
        return null;
    }

    /**
     * @return ConnectionInterface
     */
    public static function getConnection(): ConnectionInterface
    {
        return static::$CONNECTION;
    }

    /**
     * @return array
     */
    public static function getComponentCodes(): array
    {
        return array_keys(static::$CONFIG_FILES);
    }

    /**
     * @param string $code
     * @return array
     */
    public static function getComponentInstanceSettings(string $code): array
    {
        $schema = static::getSchema($code);
        if (!$schema) {
            return [];
        }

        return $schema->toArray()['instance'];
    }

    /**
     * @param string $code
     * @return Pivot|Schema|mixed|null
     */
    public static function getSchema(string $code): ?SchemaInterface
    {
        $r = null;
        if (isset(static::$schemas[$code])) {
            $r = static::$schemas[$code];
        }
        if (!($r instanceof SchemaInterface)) {
            $r = null;
        }
        return $r;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function getComponentClassName(string $code): string
    {
        $class = AbstractInstance::class;
        $config = static::getComponentInstanceSettings($code);
        if ($config['class']) {
            $class = $config['class'];
        }
        return $class;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function getComponentCreateValidationClassName(string $code): string
    {
        if (isset(static::$CONFIG_FILES[$code])
            && isset(static::$CONFIG_FILES[$code]['validations'])
            && isset(static::$CONFIG_FILES[$code]['validations']['create'])
        ) {
            return trim(static::$CONFIG_FILES[$code]['validations']['create']);
        }
        return '';
    }

    /**
     * @param string $code
     * @return string
     */
    public static function getComponentUpdateValidationClassName(string $code): string
    {
        if (isset(static::$CONFIG_FILES[$code])
            && isset(static::$CONFIG_FILES[$code]['validations'])
            && isset(static::$CONFIG_FILES[$code]['validations']['update'])
        ) {
            return trim(static::$CONFIG_FILES[$code]['validations']['update']);
        }
        return '';
    }

    /**
     * @param string $code
     * @return array
     */
    public static function getComponentCrudConfig(string $code)
    {
        $r = [];

        if (isset(static::$CONFIG_FILES[$code]) && isset(static::$CONFIG_FILES[$code]['crud']) ) {
            $r = static::$CONFIG_FILES[$code]['crud'];
        }

        if (!is_array($r)){
            $r = [];
        }

        if (!isset($r['create'])){
            $r['create'] = null;
        }

        if (!isset($r['update'])){
            $r['update'] = null;
        }

        if (!isset($r['delete'])){
            $r['delete'] = null;
        }

        return $r;
    }
}