<?php

namespace Lkt\Factory;

use Lkt\Factory\Factory\InstanceFactory;
use Lkt\Factory\LoadData\InstanceGenerator;
use Lkt\Factory\Settings\FactorySettings;

/**
 * @param $type
 * @param int $id
 * @return InstanceFactory
 */
function factory($type, int $id = 0): InstanceFactory
{
    return InstanceFactory::getInstance($type, $id);
}

/**
 * @param $type
 * @param int $id
 */
function instance($type, int $id = 0)
{
    return InstanceGenerator::getInstance($type, $id);
}


/**
 * @param string $code
 * @param string $path
 * @return void
 */
function loadConfigFile(string $code, string $path)
{
    FactorySettings::loadConfigFile($code, $path);
}


/**
 * @param string $code
 * @return array
 */
function schema(string $code): array
{
    return FactorySettings::getConfigFile($code);
}


/**
 * @param string $code
 * @return string
 */
function schemaTable(string $code): string
{
    return FactorySettings::getComponentTable($code);
}


/**
 * @param string $code
 * @return string
 */
function schemaIdColumn(string $code): string
{
    return FactorySettings::getComponentIdColumn($code);
}


/**
 * @param string $code
 * @param string $field
 * @return array
 */
function schemaColumn(string $code, string $field): array
{
    return FactorySettings::getComponentField($code, $field);
}


/**
 * @param string $code
 * @param string $fieldPivot
 * @return array
 * @deprecated
 */
function schemaPointerColumn(string $code, string $fieldPivot): ?array
{
    return FactorySettings::getComponentFieldPointingToComponent($fieldPivot, $code);
}

/**
 * @param string $str
 * @return string
 */
function escapeDatabaseCharacters(string $str): string
{
    $str = str_replace('\\', ':LKT_SLASH:', $str);
    $str = str_replace('?', ':LKT_QUESTION_MARK:', $str);
    return trim(str_replace("'", ':LKT_SINGLE_QUOTE:', $str));
}

/**
 * @param string $str
 * @return string
 */
function unescapeDatabaseCharacters(string $str): string
{
    $str = str_replace(':LKT_SLASH:', '\\', $str);
    $str = str_replace(':LKT_QUESTION_MARK:', '?', $str);
    $str = str_replace(':LKT_SINGLE_QUOTE:', "'", $str);
    return trim(str_replace('\"', '"', $str));
}
