<?php


namespace Lkt\Factory\AbstractInstances;


use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractDataParser
 *
 * @package Lkt\Factory\AbstractInstances
 * @deprecated
 */
class AbstractDatumParser
{
    use InstantiableTrait,
        AutomaticInstanceTrait;

    protected $data;
    protected $datum;
    protected $column;
    protected $field;

    /**
     * AbstractDatumParser constructor.
     *
     * @param $data
     * @param $datum
     * @param $column
     * @param $field
     */
    public function __construct(&$data, $datum, $column, $field)
    {
        $this->data = $data;
        $this->datum = $datum;
        $this->column = $column;
        $this->field = $field;
    }
}