<?php

namespace Lkt\Factory\AbstractInstances;

use Lkt\Factory\Cache\FactoryCache;
use Lkt\Factory\ColumnTypeTraits\ColumnBooleanTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnColorTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnDateTimeTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnEmailTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnFileTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnFloatTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnForeignListTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnForeignTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnIntegerTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnJsonTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnPivotTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnRelatedKeysTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnRelatedTrait;
use Lkt\Factory\ColumnTypeTraits\ColumnStringTrait;
use Lkt\Factory\FactorySettings;
use Lkt\Factory\LoadData\InstanceGenerator;
use Lkt\Factory\StoreData\StoreDataTrait;
use Lkt\InstancePatterns\AbstractInstances\AbstractCrudInstance;
use Lkt\InstancePatterns\Traits\InstantiableTrait;
use function Lkt\Factory\factory;

/**
 * Class AbstractInstance
 * @package Lkt\Factory\AbstractInstances
 */
class AbstractInstance extends AbstractCrudInstance
{
    use InstantiableTrait,
        ColumnStringTrait,
        ColumnEmailTrait,
        ColumnIntegerTrait,
        ColumnFloatTrait,
        ColumnFileTrait,
        ColumnForeignTrait,
        ColumnBooleanTrait,
        ColumnRelatedTrait,
        ColumnDateTimeTrait,
        ColumnPivotTrait,
        ColumnColorTrait,
        ColumnJsonTrait,
        ColumnForeignListTrait,
        ColumnRelatedKeysTrait,
        StoreDataTrait
        ;

    protected $TYPE;
    protected $DATA = [];
    protected $UPDATED = [];
    protected $PIVOT = [];
    protected $PIVOT_DATA = [];
    protected $UPDATED_PIVOT_DATA = [];
    protected $RELATED_DATA = [];
    protected $UPDATED_RELATED_DATA = [];
    protected $PENDING_UPDATE_RELATED_DATA = [];
    const GENERATED_TYPE = null;

    /**
     * AbstractInstance constructor.
     * @param int|string $id
     * @param null $type
     */
    public function __construct($id = null, $type = null)
    {
        if (!$type && static::GENERATED_TYPE) {
            $type = static::GENERATED_TYPE;
        }
        $this->TYPE = $type;
        if ($id > 0 || (strlen($id) > 1 && strpos($id, '-') > 0)){
            $cacheCode = "{$type}_{$id}";
            $hasToCache = false;

            if (!FactoryCache::inCache($cacheCode)) {
                factory($type, (int)$id)->query();
                $hasToCache = true;
            }

            if (FactoryCache::inCache($cacheCode)) {
                $data = FactoryCache::load($cacheCode);
                foreach ($data as $key => $datum) {
                    $this->DATA[$key] = $datum;
                }
            }

            if ($hasToCache) {
                $class = FactorySettings::getComponentClassName($type);
                if($class === get_class($this)){
                    InstanceGenerator::store($cacheCode, $this);
                }
            }

        }
    }

    /**
     * @return bool
     */
    public function isAnonymous() :bool
    {
        return count($this->DATA) === 0;
    }

    /**
     * @return mixed
     */
    public function getIdColumnValue()
    {
        $origIdColumn = FactorySettings::getComponentIdColumn(static::GENERATED_TYPE);
        return $this->DATA[$origIdColumn];
    }

    /**
     * @param string $component
     * @return $this
     */
    public function convertToComponent($component = '')
    {
        $className = FactorySettings::getComponentClassName($component);
        if (!$className) {
            return $this;
        }

        if ($this instanceof $className){
            return $this;
        }

        return $className::getInstance($this->getIdColumnValue());
    }

    /**
     * @param array $data
     */
    public function hydrate(array $data)
    {
        foreach ($data as $column => $datum){
            $this->UPDATED[$column] = $datum;
        }
    }
}